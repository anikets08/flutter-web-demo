import 'package:flutter/material.dart';
import 'package:flutterwebresponsive/constants.dart';
import 'package:flutterwebresponsive/widgets/bottom_container.dart';
import 'package:flutterwebresponsive/widgets/custom_appbar.dart';
import 'package:flutterwebresponsive/widgets/features.dart';
import 'package:flutterwebresponsive/widgets/hero.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: const [
                    SizedBox(height: 47),
                    HeroSection(),
                    FeatureSection(),
                  ],
                ),
              ),
            ),
            const CustomAppbar(),
          ],
        ),
        bottomNavigationBar:
            Constants.isWeb(context) ? null : const BottomContainer()
        // bottomNavigationBar: Container(),
        );
  }
}
