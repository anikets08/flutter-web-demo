import 'package:flutter/material.dart';

class Constants {
  static bool Function(BuildContext context) isWeb = (BuildContext context) =>
      MediaQuery.of(context).size.width > 900 ? true : false;
  static const barGradient = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color(0xFF319795),
      Color(0xFF3182CE),
    ],
  );

  static const boxGradient = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color(0xFFE6FFFA),
      Color(0xFFEBF4FF),
    ],
    stops: [0.0, 1.0],
    transform: GradientRotation(141 * 3.1415926 / 180),
  );

  static const buttonGradient = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color(0xFF319795),
      Color(0xFF3182CE),
    ],
  );
}
