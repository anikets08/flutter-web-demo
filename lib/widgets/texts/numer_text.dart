import 'package:flutter/material.dart';

class NumberText extends StatelessWidget {
  final int number;

  const NumberText({
    Key? key,
    required this.number,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Text(
        "$number.",
        style: const TextStyle(
          fontSize: 130,
          fontWeight: FontWeight.w800,
          color: Color(0xff718096),
        ),
      ),
    );
  }
}
