import 'package:flutter/material.dart';

class FeatureTitle extends StatelessWidget {
  final String title;

  const FeatureTitle({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      textAlign: TextAlign.left,
      style: const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w500,
        color: Color(0xff718096),
      ),
    );
  }
}
