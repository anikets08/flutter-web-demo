import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterwebresponsive/constants.dart';
import 'package:flutterwebresponsive/widgets/bottom_container.dart';

class HeroSection extends StatelessWidget {
  const HeroSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: WaveClipperTwo(),
      child: Container(
        padding: const EdgeInsets.only(top: 60, bottom: 100),
        // height: 100,
        width: MediaQuery.of(context).size.width,
        decoration: const BoxDecoration(
          gradient: Constants.boxGradient,
        ),
        child: Constants.isWeb(context)
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Deine Job\nwebsite",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                        ),
                      ),
                      const SizedBox(height: 20),
                      ConstrainedBox(
                          constraints: const BoxConstraints(
                            maxWidth: 300,
                          ),
                          child: HeroButton()),
                    ],
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.1),
                  Container(
                      width: 300,
                      height: 300,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: ClipRRect(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(200),
                        ),
                        child: SvgPicture.asset(
                          '/hand_shake.svg',
                          width: 300,
                        ),
                      ))
                ],
              )
            : Column(
                children: [
                  const SizedBox(height: 35),
                  const Text(
                    "Deine Job\n website",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                    ),
                  ),
                  ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: 500,
                      maxHeight: 500,
                    ),
                    child: SvgPicture.asset(
                      '/hand_shake.svg',
                      width: MediaQuery.of(context).size.width,
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
