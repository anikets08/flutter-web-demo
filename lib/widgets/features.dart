import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterwebresponsive/widgets/texts/feature_title.dart';
import 'package:flutterwebresponsive/widgets/texts/numer_text.dart';

import '../constants.dart';

const List<Map<String, dynamic>> featureTitles = [
  {
    "title": "Drei einfache Schritte zu deinem neuen Job",
    "oneImage": "/f1.svg",
    "oneTitle": "Erstellen dein Lebenslauf",
    "twoImage": "/f2.svg",
    "twoTitle": "Erstellen dein Lebenslauf",
    "threeImage": "/f3.svg",
    "threeTitle": "Mit nur einem Klick bewerben",
  },
  {
    "title": "Drei einfache Schritte zu deinem neuen Mitarbeiter",
    "oneImage": "/f1.svg",
    "oneTitle": "Erstellen dein Unternehmensprofil",
    "twoImage": "/f5.svg",
    "twoTitle": "Erstellen ein Jobinserat",
    "threeImage": "/f6.svg",
    "threeTitle": "Wähle deinen neuen Mitarbeiter aus",
  },
  {
    "title": "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter",
    "oneImage": "/f1.svg",
    "oneTitle": "Erstellen dein Unternehmensprofil",
    "twoImage": "/f8.svg",
    "twoTitle": "Erhalte Vermittlungsangebot von Arbeitgeber",
    "threeImage": "/f9.svg",
    "threeTitle": "Vermittlung nach Provision order Studenlohn",
  }
];

class FeatureSection extends StatefulWidget {
  const FeatureSection({Key? key}) : super(key: key);

  @override
  State<FeatureSection> createState() => _FeatureSectionState();
}

class _FeatureSectionState extends State<FeatureSection> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(height: 40),
        ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 700),
          child: Container(
            height: 50,
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
              gradient: Constants.boxGradient,
              border: Border.all(
                color: Colors.grey,
                width: 1,
              ),
              borderRadius: const BorderRadius.all(
                Radius.circular(15),
              ),
            ),
            child: ClipRRect(
              borderRadius: const BorderRadius.all(
                Radius.circular(15),
              ),
              child: Row(
                children: [
                  TabButtons(
                    index: 0,
                    onTap: () {
                      setState(() {
                        index = 0;
                      });
                    },
                    text: "Arbeitnehmer",
                    isSelected: index == 0,
                  ),
                  TabButtons(
                    index: 1,
                    onTap: () {
                      setState(() {
                        index = 1;
                      });
                    },
                    text: "Arbeitgeber",
                    isSelected: index == 1,
                  ),
                  TabButtons(
                    index: 2,
                    onTap: () {
                      setState(() {
                        index = 2;
                      });
                    },
                    text: "Temporärbüro",
                    isSelected: index == 2,
                  ),
                ],
              ),
            ),
          ),
        ),
        const SizedBox(height: 20),
        Constants.isWeb(context)
            ? ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 350),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    featureTitles[index]["title"],
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                    ),
                  ),
                ),
              )
            : Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  featureTitles[index]["title"],
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                  ),
                ),
              ),
        const SizedBox(height: 20),
        FeatureOne(
          image: featureTitles[index]["oneImage"],
          text: featureTitles[index]["oneTitle"],
        ),
        FeatureTwo(
          image: featureTitles[index]["twoImage"],
          text: featureTitles[index]["twoTitle"],
        ),
        FeatureThree(
          image: featureTitles[index]["threeImage"],
          text: featureTitles[index]["threeTitle"],
        ),
      ],
    );
  }
}

class FeatureOne extends StatelessWidget {
  String text;
  String image;

  FeatureOne({
    Key? key,
    required this.text,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Constants.isWeb(context)
        ? Container(
            decoration: const BoxDecoration(
              // gradient: Constants.boxGradient,
              color: Colors.white,
            ),
            child: ClipPath(
              clipper: WaveClipperTwo(
                flip: true,
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 60,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const NumberText(number: 1),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 10,
                          right: 10,
                          bottom: 30,
                        ),
                        child: FeatureTitle(title: text),
                      ),
                      const SizedBox(width: 10),
                      ConstrainedBox(
                        constraints:
                            const BoxConstraints(maxHeight: 300, maxWidth: 400),
                        child: SvgPicture.asset(
                          image,
                          width: MediaQuery.of(context).size.width,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 60,
                  )
                ],
              ),
            ),
          )
        : Container(
            decoration: const BoxDecoration(
              gradient: Constants.boxGradient,
            ),
            child: ClipPath(
              clipper: WaveClipperTwo(
                flip: true,
              ),
              child: Container(
                padding: const EdgeInsets.only(bottom: 80, top: 50),
                color: Colors.white,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const NumberText(number: 1),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ConstrainedBox(
                            constraints: const BoxConstraints(maxWidth: 500),
                            child: SvgPicture.asset(
                              image,
                              width: MediaQuery.of(context).size.width - 150,
                            ),
                          ),
                          const SizedBox(height: 30),
                          Padding(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: FeatureTitle(title: text),
                          ),
                          const SizedBox(height: 30)
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
  }
}

class FeatureTwo extends StatelessWidget {
  String text;
  String image;

  FeatureTwo({
    Key? key,
    required this.text,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Constants.isWeb(context)
        ? Container(
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: ClipPath(
              clipper: WaveClipperTwo(
                flip: true,
                reverse: true,
              ),
              child: Container(
                decoration: const BoxDecoration(
                  gradient: Constants.boxGradient,
                ),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 100,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ConstrainedBox(
                          constraints: const BoxConstraints(
                              maxHeight: 300, maxWidth: 400),
                          child: SvgPicture.asset(
                            image,
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                        const SizedBox(width: 20),
                        const NumberText(number: 2),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 10,
                            right: 10,
                            bottom: 30,
                          ),
                          child: FeatureTitle(title: text),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 60,
                    )
                  ],
                ),
              ),
            ),
          )
        : ClipPath(
            clipper: WaveClipperTwo(
              flip: true,
            ),
            child: Container(
              padding: const EdgeInsets.only(bottom: 100),
              decoration: const BoxDecoration(
                gradient: Constants.boxGradient,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const SizedBox(width: 40),
                      const NumberText(number: 2),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: FeatureTitle(title: text),
                            ),
                            const SizedBox(height: 30)
                          ],
                        ),
                      )
                    ],
                  ),
                  ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 500),
                    child: SvgPicture.asset(
                      image,
                      width: MediaQuery.of(context).size.width - 150,
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}

class FeatureThree extends StatelessWidget {
  String text;
  String image;

  FeatureThree({
    Key? key,
    required this.text,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Constants.isWeb(context)
        ? Container(
            decoration: const BoxDecoration(
              gradient: Constants.boxGradient,
            ),
            child: ClipPath(
              clipper: WaveClipperTwo(reverse: true),
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    const SizedBox(
                      height: 100,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const NumberText(number: 3),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 10,
                            right: 10,
                            bottom: 30,
                          ),
                          child: FeatureTitle(title: text),
                        ),
                        const SizedBox(width: 20),
                        ConstrainedBox(
                          constraints: const BoxConstraints(
                              maxHeight: 300, maxWidth: 400),
                          child: SvgPicture.asset(
                            image,
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 60,
                    )
                  ],
                ),
              ),
            ),
          )
        : Container(
            padding: const EdgeInsets.only(bottom: 80),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const SizedBox(width: 60),
                    const NumberText(number: 3),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: FeatureTitle(title: text),
                          ),
                          const SizedBox(height: 30)
                        ],
                      ),
                    )
                  ],
                ),
                ConstrainedBox(
                  constraints: const BoxConstraints(maxWidth: 500),
                  child: SvgPicture.asset(
                    image,
                    width: MediaQuery.of(context).size.width - 150,
                  ),
                ),
              ],
            ),
          );
  }
}

class TabButtons extends StatelessWidget {
  VoidCallback onTap;
  bool isSelected;
  String text;
  int index;

  TabButtons({
    Key? key,
    required this.onTap,
    required this.isSelected,
    required this.text,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: isSelected ? const Color(0xff81E6D9) : Colors.white,
            border: Border(
              right: BorderSide(
                color: Colors.grey,
                width: index == 1 ? 1 : 0,
              ),
              left: BorderSide(
                color: Colors.grey,
                width: index == 1 ? 1 : 0,
              ),
            ),
          ),
          child: Center(
            child: Text(
              text,
              style: TextStyle(
                color: isSelected ? Colors.white : const Color(0xff319795),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
